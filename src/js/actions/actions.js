'use strict'

import {getXml, post} from '../lib/api';
import {MenuItemModel, TextFieldModel, HiddenFieldModel} from '../lib/models';
import {List, Map} from 'immutable';


export const getMenu = (dispatch) =>
    getXml('/menu.xml').then(items => {
        const menu = items.map(s =>
            MenuItemModel({
                id: s.getAttribute('id'),
                name: s.getAttribute('name'),
                fee: Number(s.getAttribute('fee'))}))

        dispatch({
            type: 'LOAD_MENU',
            payload: menu,
        });
    })

const parseField = (f) => {
    const params = Map(
        List(f.attributes)
            .map(a => [a.name, a.value])
            .push(['name', f.nodeName]));
    return params.get('type') === 'text'
        ? TextFieldModel(params)
        : HiddenFieldModel(params);
}

const parseService = (s) =>
    List(s.querySelectorAll('*')).map(parseField);

export const getServices = (dispatch) =>
    getXml('/service.xml').then(items => {
        const services = Map(
            items.map(s =>
                [s.getAttribute('id'), parseService(s)]))

        dispatch({
            type: 'LOAD_SERVICES',
            payload: services,
        });
    })

export const selectService = (serviceId) => (dispatch) => {
    dispatch({
        type: 'SELECT_SERVICE',
        payload: serviceId,
    });
    dispatch(getServices)
}

export const pay = (fields) => (dispatch) => {
    const body = fields.map((v, k) =>
        encodeURIComponent(k) + '='+
        encodeURIComponent(v)).join('&');
    post(body);
}
