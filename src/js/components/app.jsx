import React, {Component, PropTypes} from 'react';
import {
    AppBar,
    Drawer,
    Menu,
    MenuItem,
} from 'material-ui';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import Service from '../containers/service';


class App extends Component {
    state = {
        menuOpen: true,
    }

    static childContextTypes = {
        muiTheme: React.PropTypes.object,
    }

    getChildContext() {
        return {
            muiTheme: getMuiTheme(),
        }
    }

    _toggleMenu() {
        const isOpen = !this.state.menuOpen;
        this.setState({menuOpen: isOpen});
    }

    componentDidMount() {
        this.props.getMenu();
    }

    render() {
        return <div>
            <AppBar
                title='Pay App'
                onLeftIconButtonTouchTap={::this._toggleMenu} />
            <Drawer
                docked={false}
                open={this.state.menuOpen}
                onRequestChange={::this._toggleMenu}>
                {this.props.menu.map((item, i) =>
                    <MenuItem
                        key={i}
                        onTouchTap={() => {
                            this.props.selectService(item.id);
                            this._toggleMenu();
                        }}>
                        {item.name}
                    </MenuItem>)}
            </Drawer>
            {this.props.serviceId &&
                <Service id={this.props.serviceId} />}
        </div>
    }
}

export default App;
