import React, {Component, PropTypes} from 'react';
import {
    RaisedButton,
} from 'material-ui';

const Keyboard = () => <div style={{margin: '16px 0'}}>
    <RaisedButton label='1' />
    <RaisedButton label='2' />
    <RaisedButton label='3' />
    <br />
    <RaisedButton label='4' />
    <RaisedButton label='5' />
    <RaisedButton label='6' />
    <br />
    <RaisedButton label='7' />
    <RaisedButton label='8' />
    <RaisedButton label='9' />
    <br />
    <RaisedButton label='0' style={{width: 264}}/>
</div>

export default Keyboard;
