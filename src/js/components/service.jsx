import React, {Component, PropTypes} from 'react';
import {Map} from 'immutable';
import {
    TextField,
    RaisedButton,
    FlatButton,
    Dialog,
} from 'material-ui';
import Keyboard from './keyboard';


const Field = ({field, value, isFocused, onChange}) => {
    if (field.type === 'hidden') return null;
    return <div>
        <p>{field.up_comment}</p>
        <div>
            <TextField
                id={field.name}
                value={value}
                hintText={field.left_comment}
                ref={input => input && isFocused && input.focus()}
                onChange={onChange} />
        </div>
    </div>;
}

class Service extends Component {
    _makeState(fields) {
        return {
            values: fields.map((f) => f.value || ''),
            firstInvalidFieldId: null,
            dialogOpen: null,
        }
    }

    _handleFieldChage(e, i) {
        const values = this.state.values;
        this.setState({
            values: values.set(i, e.target.value),
            firstInvalidFieldId: null,
        });
    }

    _handleNext() {
        const firstInvalidFieldId = this.state.values
            .map((value, id) => ({value, id}))
            .filter(x => x.value === '')
            .map(x => x.id)
            .get(0, null);

        this.setState({
            firstInvalidFieldId,
            dialogOpen: firstInvalidFieldId === null ? 'valid' : 'invalid',
        });
    }

    _closeDialog() {
        this.setState({dialogOpen: null});
    }

    _pay() {
        const values = this.state.values;
        const fields = Map(this.props.fields.map((f, i) => [f.name, values.get(i)]));
        const total = this.props.fee + Number(fields.get('amount', 0));

        this.props.onPay(fields.set('total', total));
        this._closeDialog();
    }

    constructor(props, context) {
        super(props, context);
        this.state = this._makeState(props.fields);
    }

    componentWillReceiveProps(newProps) {
        this.setState(this._makeState(newProps.fields));
    }

    render() {
        const {
            values,
            firstInvalidFieldId,
            invalidDialogOpen,
            dialogOpen,
        } = this.state;

        return <div style={{margin: 64}}>
            {this.props.fields.map((f, i) =>
                <Field
                    key={i}
                    value={values.get(i, '')}
                    field={f}
                    isFocused={firstInvalidFieldId === i}
                    onChange={(e) => this._handleFieldChage(e, i)} />)}
            <Keyboard />
            <RaisedButton label='next' primary={true} onClick={::this._handleNext}/>
            <Dialog
                title='Invalid dialog'
                actions={<FlatButton label='OK' onClick={::this._closeDialog} />}
                modal={true}
                open={dialogOpen === 'invalid'}>
                  {this.props.fields
                      .filter(f => f.type === 'text')
                      .map((f, i) => <p key={i}>{f.name}</p>)}
            </Dialog>
            <Dialog
                title='Valid dialog'
                actions={<FlatButton label='Pay' onClick={::this._pay} />}
                modal={true}
                open={dialogOpen === 'valid'}>
                  {this.props.fields
                      .map((f, i) => <p key={i}>{f.name} = {values.get(i)}</p>)}
                  {this.props.fields
                      .map((f, i) => encodeURIComponent(f.name)+'='+
                              encodeURIComponent(values.get(i))).join('&')}
            </Dialog>
        </div>;
    }
}

export default Service;
