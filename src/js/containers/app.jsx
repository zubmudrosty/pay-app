import {connect} from 'react-redux';
import {List} from 'immutable';

import {selectService, getMenu} from '../actions/actions';
import App from '../components/app';


const mapStateToProps = (state) => {
    const serviceId = state.app.serviceId;
    return {
        menu: state.menu,
        serviceId: state.app.serviceId,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        selectService: (id) => dispatch(selectService(id)),
        getMenu: () => dispatch(getMenu),
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(App);
