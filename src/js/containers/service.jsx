import {connect} from 'react-redux';
import {List} from 'immutable';

import {selectService, getMenu, pay} from '../actions/actions';
import Service from '../components/service';


const mapStateToProps = (state) => {
    return {
        serviceList: state.serviceList,
        menu: state.menu,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onPay: (fields) => dispatch(pay(fields)),
    }
}

const mergeProps = (stateProps, dispatchProps, ownProps) => {
    const {menu, serviceList, ...otherStateProps} = stateProps;
    const fee = menu
        .filter(x => x.id === ownProps.id)
        .map(x => x.fee)
        .get(0, 0);
    return {
        ...otherStateProps,
        ...dispatchProps,
        ...ownProps,
        fields: serviceList.get(ownProps.id, List()),
        fee,
    };
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(Service);
