'use strict'

import 'whatwg-fetch';
import {List} from 'immutable';


const getNodes = (xml) => {
    const tree = new DOMParser().parseFromString(xml, 'text/xml');
    return List(tree.documentElement.querySelectorAll('service'));
}

export const getXml = (url) => fetch(url).then((response) => {
    const body = response.text();
    if (response.status === 200 && body) {
        return Promise.resolve(body);
    } else {
        const err = new Error(`Server error: ${response.status}`);
        return Promise.reject(err);
    }
}).then(getNodes);

export const post = (body) => fetch('post.php', {
    method: 'POST',
    body,
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
    },
})
