import {List, Record} from 'immutable';

export const AppModel = Record({
    serviceId: null,
});

export const MenuItemModel = Record({
    id: null,
    name: '',
    fee: 0,
});

export const TextFieldModel = Record({
    name: '',
    left_comment: '',
    up_comment: '',
    type: 'text',
});

export const HiddenFieldModel = Record({
    name: '',
    type: 'hidden',
    value: null,
});

