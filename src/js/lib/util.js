'use strict'

import {Map, List, Set, fromJS} from 'immutable';
import {Article} from '../constants/models';

const toArray = (o) =>
    Object.keys(o).map((k) => [k, o[k]]);

const getValues = (o) =>
    Object.keys(o).map((k) => o[k]);

const flatten = (arrayOfArrays) =>
    Array.prototype.concat.apply([], arrayOfArrays);

const difference = (a, b) => {
    return Set(a).subtract(Set(b));
};

const identity = (x) => x
const constant = (x) => (y) => x

const keyValue = ([k, v]) => {
    if (Array.isArray(v)) {
        return v.map((x) => `${k}=${x}`);
    } else {
        return [`${k}=${v}`];
    }
};

const formatPrice = (x, withSign=true) => {
    return Number(x).toLocaleString("ru") + (withSign ? '\u00A0\u20BD' : '')
};

const formatDate = (x) => {
    return (new Date(x)).toLocaleDateString('ru')
};

const makeCache = (xs, id='id') => {
    return Map(xs.map((x) => [x[id], x]));
};

/**
 * Если у Record есть _makeEntity, то Record будет сконструирован
 * этой функцией
 *
 * Это нужно для Record, у которых есть вложенные Record, например, Bill
 */
const entityOf = (cls, e) => {
    const ctor = cls._makeEntity
        ? cls._makeEntity
        : (e) => new cls(fromJS(e));
    return ctor(e);
}

const listOf = (cls, array) =>
    List(array.map(x => entityOf(cls, x)));

const replaceById = (entity, list, idField = 'id') =>
    list.map((x) => x[idField] === entity[idField] ? entity : x);

const isSignature = (bill, articleCache) => bill.items.some((x) =>
    articleCache.get(x.article_id, new Article()).article_type === 'signature')

const loadDiffEntities = (getAction, reducerName, extractId) => (dispatch, getState) => (json) => {
    const _json = flatten([json]);
    const cacheIds = getState()[reducerName].get('cache').keys();
    const ids = difference(flatten(_json.map(extractId).filter(identity)), cacheIds);
    const promises = ids.map(id => dispatch(getAction(id)));
    return Promise.all(promises).then(() => json).catch((err) => {
        console.log(err);
        console.log('Ошибка получения сущностей');
        return json;
    });
}

export {
    constant,
    difference,
    flatten,
    formatDate,
    formatPrice,
    getValues,
    identity,
    isSignature,
    keyValue,
    listOf,
    entityOf,
    loadDiffEntities,
    makeCache,
    replaceById,
    toArray,
}
