import {AppModel} from '../lib/models';

const app = (state = AppModel(), action) => {
    switch (action.type) {
        case 'SELECT_SERVICE':
            return state.set('serviceId', action.payload);
        default:
            return state;
    }
}

export default app;

