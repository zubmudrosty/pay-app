import {combineReducers} from 'redux';
import menu from './menu';
import app from './app';
import serviceList from './service-list';


const rootReducer = combineReducers({
    app,
    menu,
    serviceList,
});


export default rootReducer;
