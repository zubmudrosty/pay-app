import {List} from 'immutable';

const initialState = List();

const menu = (state = initialState, action) => {
    switch (action.type) {
        case 'LOAD_MENU':
            return action.payload;
        default:
            return state;
    }
}

export default menu;
