import {Map} from 'immutable';


const initialState = Map();

const serviceList = (state = initialState, action) => {
    switch (action.type) {
        case 'LOAD_SERVICES':
            return action.payload;
        default:
            return state;
    }
}

export default serviceList;
