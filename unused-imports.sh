#!/bin/bash
function find_imports {
    awk '/import/,/;/' $1 | sed -e 's/import //g' -e 's/ from.*$//g' -e 's/,/\n/g' -e 's/[{} ]//g' | sed '/^$/d'
}

function find_unused {
    for i in $(find_imports $1)
    do
        if [ $(grep -c $i $1) == 1 ]
        then
            echo $i
        fi
    done
}


for f in $(find src/js -name '*.js' -o -name '*.jsx')
do
    UNUSED=$(find_unused $f)
    if [ "$UNUSED" ]
    then
        echo
        echo '-------------------------------------------------------------------------------'
        echo $f
        echo '-------------------------------------------------------------------------------'
        printf "$UNUSED"
        echo
        echo
    fi
done

