var webpack = require('webpack');

module.exports = require('./webpack.config')({
    devServer: {
        contentBase: 'src',
        devtool: 'eval',
        hot: true,
        inline: true,
        port: 8081,
        // proxy: { '/api/*': { target: 'http://localhost:8080' } },
    },
    devtool: 'eval',
    plugins: [
        new webpack.HotModuleReplacementPlugin()
    ]
});
