var webpack = require('webpack');
var path = require('path');
var nodeModulesPath = path.resolve(__dirname, 'node_modules');
var TransferWebpackPlugin = require('transfer-webpack-plugin');

module.exports = function(options) {
    var entry = ['babel-polyfill', path.join(__dirname, '/src/js/app.js')];
    var resolve = {
        extensions: ['', '.js', '.jsx', '.scss', '.css']
    };
    var output = {
        path: path.resolve(__dirname, 'public'),
        filename: 'js/app.js'
    };
    var plugins = [
        new webpack.NoErrorsPlugin(),
        new TransferWebpackPlugin([
            {from: 'html'}
        ], path.resolve(__dirname, 'src')),
        new TransferWebpackPlugin([
            {from: 'flexboxgrid/dist', to: 'css'}
        ], path.resolve(nodeModulesPath)),
        new TransferWebpackPlugin([
            {from: 'pdfjs-dist/build', to: 'js'}
        ], path.resolve(nodeModulesPath))

    ];
    var module = {
        preLoaders: [
            {
                test: /\.(js|jsx)$/,
                loader: 'eslint-loader',
                include: [path.resolve(__dirname, 'src/js')],
                exclude: [nodeModulesPath]
            },
        ],
        loaders: [
            {
                test: /\.(js|jsx|html)$/,
                loaders: ['react-hot', 'babel'],
                exclude: [nodeModulesPath]
            }
        ]
    };
    var eslint = {
        configFile: '.eslintrc'
    };
    var devServer = options.devServer ? options.devServer : {};

    Array.prototype.push.apply(plugins, options.plugins);

    return {
        entry: entry,
        resolve: resolve,
        output: output,
        plugins: plugins,
        module: module,
        eslint: eslint,
        devtool: options.devtool,
        devServer: devServer
    };
};
